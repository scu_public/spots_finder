function SpotsFinder
% Copyright D-BSSE, ETHZ Basel, 2018
% Supplementary code for the paper:
% Aizhan Tastanova et al.
% "A biomedical tattoo system for early detection of cancer-associated
%     hypercalcemia"

import java.awt.Color;
import javax.swing.border.LineBorder;
close all

% =========================================================================
%
% DRAW GUI
%
% =========================================================================
SS = get(0, 'ScreenSize');
figure('outerposition', ...
    [0.1 * SS(3) 0.1 * SS(4) 0.8*SS(3) 0.9 * SS(4)], ...
    'Name', 'Spot detection', 'NumberTitle', 'off', 'Toolbar', 'figure');

% First panel - load images Control
% -------------------------------------------------------------------------
panel1 = uipanel('Units', 'normalized', ...
    'Position', [0.01 0.01 0.33 0.99], 'Title', 'Control Images', ...
    'backgroundcolor', [0.95 0.87 0.73]);
h.FileBtn = uicontrol('parent', panel1, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.08 0.95 0.3 0.04], ...
    'FontSize', 9.9999, 'String', 'Load control images', ...
    'backgroundcolor', [0 0.75 0.75], 'Callback', {@FileBtn_Callback});
h.showfilename = uicontrol('parent', panel1, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.4 0.96 0.4 0.02], ...
    'backgroundcolor', [0.95 0.87 0.73], 'String', 'filename', ...
    'fontsize', 7);
h.Image = axes('Units', 'normalized', 'Position', [0.05 0.18 0.8 0.8], ...
    'FontSize', 1, 'parent', panel1); axis square
h.PrevImgBtn = uicontrol('parent', panel1, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.15 0.12 0.25 0.05], ...
    'String', '< Previous image', 'backgroundcolor', [0.7 0.8 0.6], ...
    'Callback', {@PrevImgBtn_Callback});
h.NextImgBtn = uicontrol('parent', panel1, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.5 0.12 0.25 0.05], ...
    'String', 'Next image >', 'backgroundcolor', [0.7 0.8 0.6], ...
    'Callback', {@NextImgBtn_Callback});
uicontrol('parent', panel1, 'Style', 'text', 'Units', 'normalized', ...
    'Position', [0.3 0.86 0.08 0.04], ...
    'BackgroundColor', [0.95 0.87 0.73], 'String', 'R=', 'fontsize', 12);
h.REdt = uicontrol('parent', panel1, 'Style', 'edit', ...
    'Units', 'normalized', 'Position', [0.38 0.875 0.08 0.03], ...
    'fontsize', 13, 'String', '30', 'backgroundcolor', 'w');
h.SelectSpotBtn = uicontrol('parent', panel1, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.05 0.87 0.2 0.04], ...
    'String', 'Select spot', 'backgroundcolor', [0.7 0.9 0.7], ...
    'Callback', {@SelectSpotBtn_Callback});
h.MeanSpotInt1 = uicontrol('parent', panel1, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.48 0.872 0.2 0.03], ...
    'BackgroundColor', [0.95 0.87 0.73], 'String', '<Spot>=?', ...
    'fontsize', 8);
h.SelectPaperBtn = uicontrol('parent', panel1, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.05 0.81 0.2 0.04], ...
    'String', 'Select paper', 'backgroundcolor', [0.6 0.9 0.7], ...
    'Callback', {@SelectPaperBtn_Callback});
h.MeanBlackInt1 = uicontrol('parent', panel1, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.25 0.81 0.2 0.03], ...
    'BackgroundColor', [0.95 0.87 0.73], 'String', '<Black>=?', ...
    'fontsize', 8);
h.ShowImgNum1 = uicontrol('parent', panel1, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.25 0.3 0.5 0.03], ...
    'BackgroundColor', [0.95 0.87 0.73], 'String', '(o_O)', ...
    'fontsize', 12, 'Visible', 'off');
h.SkinKoefEdt = uicontrol('parent', panel1, 'Style', 'edit', ...
    'Units', 'normalized', 'Position', [0.9 0.875 0.06 0.03], ...
    'fontsize', 13, 'String', '4', 'backgroundcolor', [0.6 0.6 0.6]);
h.ShowGroupControl = uicontrol('parent', panel1, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.25 0.05 0.9 0.03], ...
    'BackgroundColor', [0.95 0.87 0.73], ...
    'String', '<Here will be group data>', 'fontsize', 12);
uicontrol('parent', panel1, 'Style', 'text', 'Units', 'normalized', ...
    'Position', [0.74 0.87 0.15 0.028], ...
    'BackgroundColor', [0.95 0.87 0.73], 'String', 'R_skin=', ...
    'fontsize', 12);

% First panel - load images Data
% -------------------------------------------------------------------------
panel2 = uipanel('Units', 'normalized', ...
    'Position', [0.34 0.01 0.33 0.99], 'Title', 'Data Images', ...
    'backgroundcolor', [0.9 0.8 0.73]);
h.FileBtnData = uicontrol('parent',panel2, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.08 0.95 0.3 0.04], ...
    'FontSize',9.9999, 'String', 'Load data images', ...
    'backgroundcolor', [0 0.75 0.75], 'Callback', {@FileBtnData_Callback});
h.showfilenameData = uicontrol('parent',panel2, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.4 0.96 0.4 0.02], ...
    'backgroundcolor', [0.9 0.8 0.73], 'String', 'filename', 'fontsize',7);
h.ImageData = axes('Units', 'normalized', ...
    'Position', [0.05 0.18 0.8 0.8], 'FontSize', 1, 'parent', panel2);...
    axis square
h.PrevImgBtnData = uicontrol('parent',panel2, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.15 0.12 0.25 0.05], ...
    'String', '< Previous image', 'backgroundcolor', [0.7 0.8 0.6], ...
    'Callback', {@PrevImgBtnData_Callback});
h.NextImgBtnData = uicontrol('parent',panel2, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.5 0.12 0.25 0.05], ...
    'String', 'Next image >', 'backgroundcolor', [0.7 0.8 0.6], ...
    'Callback', {@NextImgBtnData_Callback});
uicontrol('parent',panel2, 'Style', 'text', 'Units', 'normalized', ...
    'Position', [0.3 0.86 0.08 0.04], ...
    'BackgroundColor', [0.9 0.8 0.73], 'String', 'R=', 'fontsize',12);
h.REdtData = uicontrol('parent',panel2, 'Style', 'edit', ...
    'Units', 'normalized', 'Position', [0.38 0.872 0.08 0.03], ...
    'fontsize', 13, 'String', '30', 'backgroundcolor', 'w');
h.SelectSpotBtnData = uicontrol('parent',panel2, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.05 0.87 0.2 0.04], ...
    'String', 'Select spot', 'backgroundcolor', [0.7 0.9 0.7], ...
    'Callback', {@SelectSpotBtnData_Callback});
h.MeanSpotInt2 = uicontrol('parent',panel2, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.48 0.872 0.2 0.03], ...
    'BackgroundColor', [0.9 0.8 0.73], 'String', '<Spot>=?', ...
    'fontsize',8);
h.SelectPaperBtnData = uicontrol('parent',panel2, ...
    'Style', 'pushbutton', 'Units', 'normalized', ...
    'Position', [0.05 0.81 0.2 0.04], 'String', 'Select paper', ...
    'backgroundcolor', [0.6 0.9 0.7], ...
    'Callback', {@SelectPaperBtnData_Callback});
h.MeanBlackInt2 = uicontrol('parent',panel2, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.25 0.81 0.2 0.03], ...
    'BackgroundColor', [0.9 0.8 0.73], 'String', '<Black>=?', ...
    'fontsize',8);
h.ShowImgNum2 = uicontrol('parent',panel2, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.45 0.3 0.5 0.03], ...
    'BackgroundColor', [0.9 0.8 0.73], 'String', '(o_O)', ...
    'fontsize',12, 'Visible', 'off');
h.ShowGroupData = uicontrol('parent',panel2, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.25 0.05 0.9 0.03], ...
    'BackgroundColor', [0.9 0.8 0.73], ...
    'String', '<Here will be group data>', 'fontsize', 12);
h.SkinKoefDataEdt = uicontrol('parent',panel2, 'Style', 'edit', ...
    'Units', 'normalized', 'Position', [0.9 0.875 0.06 0.03], ...
    'fontsize',13, 'String', '4', 'backgroundcolor', [0.6 0.6 0.6]);
uicontrol('parent',panel2, 'Style', 'text', 'Units', 'normalized', ...
    'Position', [0.74 0.87 0.15 0.028], ...
    'BackgroundColor', [0.9 0.8 0.73], 'String', 'R_skin=', ...
    'fontsize',12);

% Third panel - manipulate images, get data
% -------------------------------------------------------------------------
panel3 = uipanel('Units', 'normalized', ...
    'Position', [0.67 0.01 0.33 0.99], 'Title', 'Output Data', ...
    'backgroundcolor', [0.73 0.83 0.96]);
h.CalculateAllBtn = uicontrol('parent', panel3, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.25 0.9 0.3 0.05], ...
    'String', 'Calculate all', 'backgroundcolor', [0.7 0.6 0.7], ...
    'Callback', {@CalculateAllBtn_Callback});
h.StatisticResult = uicontrol('parent', panel3, 'Style', 'text', ...
    'Units', 'normalized', 'Position', [0.15 0.75 0.5 0.1], ...
    'String', '', 'fontsize', 12, 'HorizontalAlignment', 'left', ...
    'backgroundcolor', [0.73 0.83 0.96]);
h.StatisticAxes = axes('parent', panel3, 'Units', 'normalized', ...
    'Position', [0.1 0.1 0.8 0.4], 'FontSize',10);
h.SaveAllBtn = uicontrol('parent', panel3, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.7 0.03 0.2 0.05], ...
    'String', 'Save all', 'backgroundcolor', [0.7 0.6 0.8], ...
    'Callback', {@SaveAllBtn_Callback});
h.BoxAxes = axes('parent', panel3, 'Units', 'normalized', ...
    'Position', [0.2 0.53 0.45 0.1], 'FontSize', 10);

% Current plots
h.rect = [];
h.circle =  [];
h.circleSkin = [];
h.rectData = [];
h.circleData = [];

% All plots
h.rectAll = {};
h.circleAll =  {};
h.circleSkinAll = {};
h.rectDataAll = {};
h.circleDataAll = {};
h.circleSkinDataAll = {};

% =========================================================================
%
% CALLBACKS
%
% =========================================================================

% Select control images
% -------------------------------------------------------------------------
    function FileBtn_Callback(~, ~)
        % Open files from finder
        [h.filename, h.pathname] = uigetfile('*.tif', ...
            'Select .tif file', 'MultiSelect', 'on');

        % Load all files
        if ~iscell(h.filename)
            h.filename = {h.filename};
        end

        % Set some enviromental variables and visualize ui variables if they
        % are avaliable
        set(h.showfilename, 'String', h.filename(1, 1));
        h.step = 0;
        h.ImgNum = 1;
        h.imgMatrix = im2double(imread(fullfile(h.pathname, ...
            cell2mat(h.filename(1, h.ImgNum)))));
        imshow(h.imgMatrix, [], 'parent', h.Image);
        h.Norm = zeros(size(h.filename, 2), 1);
        set(h.ShowImgNum1, 'String', ['Showing ',num2str(h.ImgNum), ...
            ' out of ',num2str(size(h.filename, 2))])
        set(h.ShowImgNum1, 'Visible', 'on')
        h.Spots = cell(1, size(h.filename, 2));
        set(h.MeanSpotInt1, 'String', '<Spot>=?');
        set(h.MeanBlackInt1, 'String',strcat('<Black>=?'));
    end

% Go to next control image
% -------------------------------------------------------------------------
    function NextImgBtn_Callback(~, ~)
        % Check if we are not exceeding number of images
        if h.ImgNum >= size(h.filename, 2)
            return;
        end

        % Increment image number
        h.ImgNum = h.ImgNum + 1;

        % Load image
        if iscell(h.filename) == 1
            h.imgMatrix = im2double(imread(fullfile(h.pathname, ...
                cell2mat(h.filename(1, h.ImgNum)))));
            imshow(h.imgMatrix, [], 'parent', h.Image);
            set(h.showfilename, 'String', h.filename(1, h.ImgNum));
        end

        %Set variables shown in ui if they are avaliable
        set(h.ShowImgNum1, 'String', ['Showing ', num2str(h.ImgNum), ...
            ' out of ',num2str(size(h.filename,2))])
        if isnan(h.Spots{1, h.ImgNum}) == 0
            set(h.MeanSpotInt1, 'String', ...
                strcat('<Spot>=', num2str(mean(h.Spots{1, h.ImgNum}))));
        else
            set(h.MeanSpotInt1, 'String', '<Spot>=?');

        end
        if h.Norm(h.ImgNum) ~= 0
            set(h.MeanBlackInt1, 'String', ...
                strcat('<Black>=', num2str(h.Norm(h.ImgNum))));
        else
            set(h.MeanBlackInt1, 'String', strcat('<Black>=?'));
        end

        % Draw paper and spot selections
        hold(h.Image, 'on');

        % Spot circle
        if numel(h.circleAll) >= h.ImgNum && ...
                ~isempty(h.circleAll{h.ImgNum})
            h.circle =  viscircles( ...
                h.circleAll{h.ImgNum}(1:2), ...
                h.circleAll{h.ImgNum}(3), ...
                'EdgeColor', 'r', 'LineWidth', 0.2);
            h.circle.Parent = h.Image;
        end

        % Spot skin circle
        if numel(h.circleSkinAll) >= h.ImgNum && ...
                ~isempty(h.circleSkinAll{h.ImgNum})
            h.circleSkin = viscircles( ...
                h.circleSkinAll{h.ImgNum}(1:2), ...
                h.circleSkinAll{h.ImgNum}(3), ...
                'EdgeColor', 'g', 'LineWidth', 0.2);
            h.circleSkin.Parent = h.Image;
        end

        % Paper rectangle
        if numel(h.rectAll) >= h.ImgNum && ...
                ~isempty(h.rectAll{h.ImgNum})
            h.rect = rectangle('Position', h.rectAll{h.ImgNum}, ...
                'parent', h.Image, 'EdgeColor', 'b');
        end

        hold(h.Image, 'off');

    end

% Go to previous control image
% -------------------------------------------------------------------------
    function PrevImgBtn_Callback(~, ~)
        % Check if are not going below 1 with image number
        if h.ImgNum <= 1
            return;
        end

        % Decrement image number
        h.ImgNum = h.ImgNum - 1;

        % Load image
        if iscell(h.filename) == 1 && h.ImgNum > 0
            h.imgMatrix = im2double(imread( ...
                fullfile(h.pathname, cell2mat(h.filename(1, h.ImgNum)))));
            imshow(h.imgMatrix, [], 'parent', h.Image);
            set(h.showfilename, 'String', h.filename(1, h.ImgNum));
        end

        % Set variables shown in ui if they are avaliable
        set(h.ShowImgNum1, 'String', ['Showing ',num2str(h.ImgNum), ...
            ' out of ', num2str(size(h.filename, 2))])
        if isnan(h.Spots{1, h.ImgNum}) == 0
            set(h.MeanSpotInt1, 'String', ...
                strcat('<Spot>=', num2str(mean(h.Spots{1, h.ImgNum}))));
        else
            set(h.MeanSpotInt1, 'String', '<Spot>=?');
        end
        if h.Norm(h.ImgNum) ~= 0
            set(h.MeanBlackInt1, 'String', ...
                strcat('<Black>=', num2str(h.Norm(h.ImgNum))));
        else
            set(h.MeanBlackInt1, 'String', strcat('<Black>=?'));
        end

        % Draw paper and spot selections
        hold(h.Image, 'on');

        % Spot circle
        if numel(h.circleAll) >= 1 && ...
                ~isempty(h.circleAll{h.ImgNum})
            h.circle =  viscircles( ...
                h.circleAll{h.ImgNum}(1:2), ...
                h.circleAll{h.ImgNum}(3), ...
                'EdgeColor', 'r', 'LineWidth', 0.2);
            h.circle.Parent = h.Image;
        end

        % Spot skin circle
        if numel(h.circleSkinAll) >= 1 && ...
                ~isempty(h.circleSkinAll{h.ImgNum})
            h.circleSkin = viscircles( ...
                h.circleSkinAll{h.ImgNum}(1:2), ...
                h.circleSkinAll{h.ImgNum}(3), ...
                'EdgeColor', 'g', 'LineWidth', 0.2);
            h.circleSkin.Parent = h.Image;
        end

        % Paper rectangle
        if numel(h.rectAll) >= 1 && ~isempty(h.rectAll{h.ImgNum})
            h.rect = rectangle('Position', h.rectAll{h.ImgNum}, ...
                'parent', h.Image, 'EdgeColor', 'b');
        end

        hold(h.Image, 'off');

    end

% Select spot in control image
% -------------------------------------------------------------------------
    function SelectSpotBtn_Callback(~, ~)
        % Remove existing spots
        try
            delete(h.circle);
            delete(h.circleSkin);
        catch
        end

        % Get parameters for the spot size and coefficient for circle
        % expansion for skin calculations
        h.r = str2double(get(h.REdt, 'String'));
        SkinKoef = str2double(get(h.SkinKoefEdt, 'String'));

        % Get user input
        axes(h.Image);
        ix = size(h.imgMatrix, 1);
        iy = size(h.imgMatrix, 1);
        [cx,cy] = ginput(1);
        h.cx = round(cx);
        h.cy = round(cy);

        % Draw circle
        hold(h.Image, 'on');
        h.circle =  viscircles([cx, cy], h.r, ...
            'EdgeColor', 'r', 'LineWidth', 0.2);
        hold(h.Image, 'off');

        % Extract image inside selected spot
        [x, y] = meshgrid(-(h.cx - 1) : (ix - h.cx), ...
            -(h.cy - 1) : (iy - h.cy));
        c_mask = ((x.^2 + y.^2) <= h.r^2);
        h.mask = c_mask .* h.imgMatrix;
        [~, ~, v] = find(h.mask);

        % Add data to the list
        h.Spots(h.ImgNum) = {v};

        % Store the circle info
        h.circleAll{h.ImgNum} = [cx cy h.r];

        % Get the info on the skin color -> enlarge initial circle by
        % expansion coefficient defined by user
        c_maskSkin = ((x.^2 + y.^2) <= (h.r + SkinKoef * h.r)^2);
        h.maskSkin = (c_maskSkin .* h.imgMatrix) - h.mask;
        [~,~,v] = find(h.maskSkin);

        % Calculate median of the skin intensity
        meanSkin = median(v);

        % Draw the area used for skin calculations
        hold(h.Image, 'on');
        h.circleSkin = viscircles([cx, cy], (h.r + SkinKoef * h.r), ...
            'EdgeColor', 'g', 'LineWidth', 0.2);
        hold(h.Image, 'off');

        % Normalize for skin
        h.Spots{1, h.ImgNum} = h.Spots{1, h.ImgNum} - meanSkin;
        set(h.MeanSpotInt1, 'String', ...
            strcat('<Spot>=', num2str(mean(h.Spots{1, h.ImgNum}))));

        % Store the circle skin info
        h.circleSkinAll{h.ImgNum} = [cx cy (h.r + SkinKoef * h.r)];

    end

% Select paper reference in control image
% -------------------------------------------------------------------------
    function SelectPaperBtn_Callback(~, ~)
        % Remove existing rectangles
        try
            delete(h.rect);
        catch
        end

        % Get user input
        axes(h.Image);
        rect = getrect(h.Image);

        % Associate user input with paper image
        x1 = round(rect(1));
        y1 = round(rect(2));
        x2 = round(rect(1) + rect(3));
        y2 = round(rect(4) + rect(2));

        % Draw the rectangle
        hold(h.Image, 'on');
        h.rect = rectangle('Position', [x1 y1 rect(3) rect(4)], ...
            'parent', h.Image, 'EdgeColor', 'b');
        hold(h.Image, 'off');

        % Add mean intensity of the paper image to the corresponding list
        h.Norm(h.ImgNum) = ...
            mean(mean(h.imgMatrix(y1 : y2, x1 : x2)));

        % Show the mean in the ui
        set(h.MeanBlackInt1, 'String', strcat('<Black>=', ...
            num2str(h.Norm(h.ImgNum))) );

        % Store the rectangle info
        h.rectAll{h.ImgNum} = [x1 y1 (x2 - x1) (y2 - y1)];

    end

% Pick data images
% -------------------------------------------------------------------------
    function FileBtnData_Callback(~, ~)
        % Open files from finder
        [h.filenamedata, h.pathnamedata] = uigetfile('*.tif', ...
            'Select .tif file', 'MultiSelect', 'on');

        % Load all files
        if ~iscell(h.filenamedata)
            h.filenamedata = {h.filenamedata};
        end

        % Set some enviromental variables and visualize ui variables if they
        % are avaliable
        set(h.showfilenameData, 'String', h.filenamedata(1, 1));
        h.step = 0;
        h.ImgNumData = 1;
        h.imgMatrixData = im2double(imread(fullfile(h.pathnamedata, ...
            cell2mat(h.filenamedata(1, h.ImgNumData)))));
        imshow(h.imgMatrixData, [], 'parent', h.ImageData);
        h.NormData = zeros(size(h.filenamedata, 2), 1);
        set(h.ShowImgNum2, 'String', ['Showing ', num2str(h.ImgNumData), ...
            ' out of ', num2str(size(h.filenamedata, 2))])
        set(h.ShowImgNum2, 'Visible', 'on')
        h.SpotsData = cell(1,size(h.filenamedata, 2));
        set(h.MeanSpotInt2, 'String', '<Spot>=?');
        set(h.MeanBlackInt2, 'String', strcat('<Black>=?'));
    end

% Go to next data image
% -------------------------------------------------------------------------
    function NextImgBtnData_Callback(~, ~)
        % Check if we are not exceeding number of images
        if h.ImgNumData >= size(h.filenamedata,2)
            return;
        end

        % Increment image number
        h.ImgNumData = h.ImgNumData+1;

        % Load image
        if iscell(h.filenamedata) == 1
            h.imgMatrixData = im2double(imread(fullfile(h.pathnamedata, ...
                cell2mat(h.filenamedata(1, h.ImgNumData)))));
            imshow(h.imgMatrixData, [], 'parent', h.ImageData);
        end

        % Set variables shown in ui if they are avaliable
        set(h.ShowImgNum2, 'String', ['Showing ', num2str(h.ImgNumData), ...
            ' out of ',num2str(size(h.filenamedata, 2))])
        if isnan(h.SpotsData{1, h.ImgNumData}) == 0
            set(h.MeanSpotInt2, 'String', strcat('<Spot>=', ...
                num2str(mean(h.SpotsData{1, h.ImgNumData}))));
        else
            set(h.MeanSpotInt2, 'String', '<Spot>=?');
        end
        if h.NormData(h.ImgNumData) ~= 0
            set(h.MeanBlackInt2, 'String', ...
                strcat('<Black>=', num2str(h.NormData(h.ImgNumData))));
        else
            set(h.MeanBlackInt2, 'String', strcat('<Black>=?'));
        end

        % Draw paper and spot selections
        hold(h.ImageData, 'on');

        % Spot circle
        if numel(h.circleDataAll) >= h.ImgNumData && ...
                ~isempty(h.circleDataAll{h.ImgNumData})
            h.circleData =  viscircles( ...
                h.circleDataAll{h.ImgNumData}(1:2), ...
                h.circleDataAll{h.ImgNumData}(3), ...
                'EdgeColor', 'r', 'LineWidth', 0.2);
            h.circleData.Parent = h.ImageData;
        end

        % Spot skin circle
        if numel(h.circleSkinDataAll) >= h.ImgNumData && ...
                ~isempty(h.circleSkinDataAll{h.ImgNumData})
            h.circleSkinData = viscircles( ...
                h.circleSkinDataAll{h.ImgNumData}(1:2), ...
                h.circleSkinDataAll{h.ImgNumData}(3), ...
                'EdgeColor', 'g', 'LineWidth', 0.2);
            h.circleSkinData.Parent = h.ImageData;
        end

        % Paper rectangle
        if numel(h.rectDataAll) >= h.ImgNumData && ...
                ~isempty(h.rectDataAll{h.ImgNumData})
            h.rectData = rectangle('Position', ...
                h.rectDataAll{h.ImgNumData}, ...
                'parent', h.ImageData, 'EdgeColor', 'b');
        end

        hold(h.ImageData, 'off');

    end

% Go to previous data image
% -------------------------------------------------------------------------
    function PrevImgBtnData_Callback(~, ~)
        % Check if are not going below 1 with image number
        if h.ImgNumData <= 1
            return;
        end

        % Decrement image number
        h.ImgNumData = h.ImgNumData - 1;

        % Load image
        if iscell(h.filenamedata) == 1 && h.ImgNumData > 0
            h.imgMatrixData = im2double(imread(fullfile(h.pathnamedata, ...
                cell2mat(h.filenamedata(1, h.ImgNumData)))));
            imshow(h.imgMatrixData, [], 'parent', h.ImageData);
        end

        % Set variables shown in ui if they are avaliable
        set(h.ShowImgNum2, 'String', ['Showing ', num2str(h.ImgNumData), ...
            ' out of ',num2str(size(h.filenamedata, 2))])
        if isnan(h.SpotsData{1, h.ImgNumData}) == 0
            set(h.MeanSpotInt2, 'String', strcat('<Spot>=', ...
                num2str(mean(h.SpotsData{1, h.ImgNumData}))));
        else
            set(h.MeanSpotInt2, 'String', '<Spot>=?');

        end
        if h.NormData(h.ImgNumData) ~= 0
            set(h.MeanBlackInt2, 'String', strcat('<Black>=', ...
                num2str(h.NormData(h.ImgNumData))));
        else
            set(h.MeanBlackInt2, 'String', strcat('<Black>=?'));
        end

        % Draw paper and spot selections
        hold(h.ImageData, 'on');

        % Spot circle
        if numel(h.circleDataAll) >= 1 && ...
                ~isempty(h.circleDataAll{h.ImgNumData})
            h.circleData =  viscircles( ...
                h.circleDataAll{h.ImgNumData}(1:2), ...
                h.circleDataAll{h.ImgNumData}(3), ...
                'EdgeColor', 'r', 'LineWidth', 0.2);
            h.circleData.Parent = h.ImageData;
        end

        % Spot skin circle
        if numel(h.circleSkinDataAll) >= 1 && ...
                ~isempty(h.circleSkinDataAll{h.ImgNumData})
            h.circleSkinData = viscircles( ...
                h.circleSkinDataAll{h.ImgNumData}(1:2), ...
                h.circleSkinDataAll{h.ImgNumData}(3), ...
                'EdgeColor', 'g', 'LineWidth', 0.2);
            h.circleSkinData.Parent = h.ImageData;
        end

        % Paper rectangle
        if numel(h.rectDataAll) >= 1 && ...
                ~isempty(h.rectDataAll{h.ImgNumData})
            h.rectData = rectangle('Position', ...
                h.rectDataAll{h.ImgNumData}, ...
                'parent', h.ImageData, 'EdgeColor', 'b');
        end

        hold(h.ImageData, 'off');

    end

% Select spot in data image
% -------------------------------------------------------------------------
    function SelectSpotBtnData_Callback(~, ~)
        % Remove existing spots
        try
            delete(h.circleData);
            delete(h.circleSkinData);
        catch
        end

        % Get parameters for the spot size and coefficient for circle
        % expansion for skin calculations
        h.rData = str2double(get(h.REdtData, 'String'));
        SkinKoef = str2double(get(h.SkinKoefDataEdt, 'String'));

        % Get user input
        axes(h.ImageData);
        ix = size(h.imgMatrixData, 1);
        iy = size(h.imgMatrixData, 1);
        [cx, cy] = ginput(1);
        h.cxData = round(cx);
        h.cyData = round(cy);

        % Draw circle
        hold(h.ImageData, 'on');
        h.circleData = viscircles([cx, cy], h.rData, 'EdgeColor', 'r', ...
            'LineWidth', 0.2);
        hold(h.ImageData, 'off');

        % Extract image inside selected spot
        [x, y] = meshgrid(-(h.cxData - 1) : (ix - h.cxData), ...
            -(h.cyData - 1) : (iy - h.cyData));
        c_maskData = ((x.^2 + y.^2) <= h.rData^2);
        h.maskData = c_maskData .* h.imgMatrixData;
        [~, ~, v] = find(h.maskData);

        % Add data to the list
        h.SpotsData(h.ImgNumData) = {v};
        
        % Store the circle info
        h.circleDataAll{h.ImgNumData} = [cx cy h.rData];
        
        % Get the info on the skin color -> enlarge initial circle by
        % expansion coefficient defined by user
        c_maskSkin = ((x.^2 + y.^2) <= (h.rData + SkinKoef * h.rData)^2);
        h.maskSkinData = (c_maskSkin .* h.imgMatrixData) - h.maskData;
        [~, ~, v] = find(h.maskSkinData);

        % Calculate median of the skin intensity
        meanSkin = median(v);

        % Draw the area used for skin calculations
        hold(h.ImageData, 'on');
        h.circleSkinData = viscircles([cx, cy], ...
            (h.rData + SkinKoef * h.rData), 'EdgeColor', 'g', ...
            'LineWidth', 0.2);
        hold(h.Image, 'off');

        % Normalize for skin
        h.SpotsData{1, h.ImgNumData} = ...
            h.SpotsData{1, h.ImgNumData} - meanSkin;
        set(h.MeanSpotInt2, 'String', strcat('<Spot>=', ...
            num2str(mean(h.SpotsData{1, h.ImgNumData}))));

        % Store the circle skin info
        h.circleSkinDataAll{h.ImgNumData} = ...
            [cx cy (h.rData + SkinKoef * h.rData)];
    end

% Select paper reference in data image
% -------------------------------------------------------------------------
    function SelectPaperBtnData_Callback(~, ~)
        % Remove existing rectangles
        try
            delete(h.rectData);
        catch
        end

        % Get user input
        axes(h.ImageData);
        rect = getrect(h.ImageData);

        % Associate user input with paper image
        x1 = round(rect(1));
        y1 = round(rect(2));
        x2 = round(rect(1) + rect(3));
        y2 = round(rect(4) + rect(2));

        % Draw the rectangle
        hold(h.ImageData, 'on');
        h.rectData = rectangle('Position', [x1 y1 rect(3) rect(4)], ...
            'parent', h.ImageData, 'EdgeColor', 'b');
        hold(h.ImageData, 'off');

        % Add mean intensity of the paper image to the corresponding list
        h.NormData(h.ImgNumData) = ...
            mean(mean(h.imgMatrixData(y1 : y2, x1 : x2)));

        % Show the mean in the ui
        set(h.MeanBlackInt2, 'String',strcat('<Black>=', ...
            num2str(h.NormData(h.ImgNumData))) );

        % Store the rectangle info
        h.rectDataAll{h.ImgNumData} = [x1 y1 (x2 - x1) (y2 - y1)];

    end

% Perform calculation on selected control and data intensities
% -------------------------------------------------------------------------
    function CalculateAllBtn_Callback(~, ~)

        %
        % Control images
        %

        % Set output control variable to be empty
        h.ControldataOut = [];

        % Check if paper controls were set
        if isfield(h, 'Norm')

            % Take mean of all control paper measurements
            h.MeanNorm = mean(h.Norm);

            % Normalize each control spot
            Controldata = cell(1, size(h.filename, 2));
            for i = 1 : size(h.filename, 2)
                % Equalize intensities of each pixel in the current spot
                Controldata(i) = {h.Spots{1, i}' / h.Norm(i) * h.MeanNorm};

                % Check if data is distributed normally
                % if kstest(Controldata{i}, normscdf(Controldata{i}, ...
                %     mean(Controldata{i}, std(Controldata{i})))) == 0
                % Get mean intensity of the current spot
                h.ControldataPerMouse(i) = mean(Controldata{i}); %If data is distributed normally
                % end
            end

            % Add all pixel intensities of all spots to one vector
            h.ControldataOut = cell2mat(Controldata);
        end

        %
        % Data images
        %

        % Set output data variable to be empty
        h.dataOut = [];

        % Check if paper controls were set
        if isfield(h, 'NormData')
            % Take mean of all control paper measurements
            h.MeanNormData = mean(h.NormData);
            
            % Normalize each control spot
            data = cell(1, size(h.filenamedata, 2));
            for i = 1:size(h.filenamedata, 2)
                % Equalize intensities of each pixel in the current spot
                data(i) = {h.SpotsData{1,i}' / h.NormData(i) * h.MeanNormData};
            
                % Check if data is distributed normally
                % if kstest(data{i}, normscdf(data{i}, ...
                %     mean(data{i},std(data{i})))) == 0
                % Get mean intensity of the current spot
                h.dataPerMouse(i) = mean(data{i}); % If data is distributed normally
                % end
            end
            
            % Add all pixel intensities of all spots to one vector
            h.dataOut = cell2mat(data);
        end

        %
        % Visualize data
        %
        
        % Plot data per mouse
        % Control: standard Error, T-Score and Confidence Interval
        SEM = std(h.ControldataPerMouse) / ...
            sqrt(length(h.ControldataPerMouse));
        ts = tinv([0.025  0.975], length(h.ControldataPerMouse) - 1);
        CIControldataPerMouse = mean(h.ControldataPerMouse) + ts * SEM;

        % Data: standard Error, T-Score and Confidence Interval
        SEM = std(h.dataPerMouse) / sqrt(length(h.dataPerMouse));
        ts = tinv([0.025  0.975], length(h.dataPerMouse) - 1);
        CIdataPerMouse = mean(h.dataPerMouse) + ts * SEM;

        % Make bar plot of the mean data and errorbars representing
        % confidence interval
        bar([mean(h.ControldataPerMouse) mean(h.dataPerMouse)], ...
            'FaceColor', 'w', 'parent', h.BoxAxes);
        title('Data grouped by mouse')
        hold(h.BoxAxes, 'on')
        errorbar([mean(h.ControldataPerMouse) mean(h.dataPerMouse)], ...
            [CIControldataPerMouse(1) CIdataPerMouse(1)], ...
            'parent', h.BoxAxes)
        %errorbar(mean(h.dataPerMouse),CIdataPerMouse(1), ...
        %    'parent', h.BoxAxes)
        hold(h.BoxAxes, 'off')
        set(h.BoxAxes, 'xticklabel', {'control', 'data'})

        % Plot histogram of all datapoints
        axes(h.StatisticAxes); cla;
        hold on;
        if ~isempty(h.ControldataOut)
            histogram(h.ControldataOut);
        end
        if ~isempty(h.dataOut)
            histogram(h.dataOut);
            title('Histogram of all data points')
        end
        hold off;

        %
        % Statistical analysis
        %
        
        % Check if data for both control and experiment exist
        if ~isempty(h.ControldataPerMouse) && ~isempty(h.dataPerMouse)
            % Use two-sample t-test to compare means of two distributions with unequal variances
            % at a significance level of 0.05 with alternative hypothesis 
            % that the population means are not equal
            [res, p, ci] = ttest2(h.ControldataPerMouse, ...
                h.dataPerMouse, 'alpha', 0.05, 'tail', 'both', ...
                'VarType', 'unequal');

            % Output an outcome of the t-test
            outcome = {'false', 'true'};
            resText = {'Statistics', ' ', ...
                ['H0 rejected: ', outcome{res + 1}], ...
                ['p value = ', num2str(p)], ['Confidence interval: ', ...
                '[', num2str(ci(1)), '; ', num2str(ci(2)), ']']};
            set(h.StatisticResult, 'String', resText);
        end

        % Show mean and SD for groups in the ui
        set(h.ShowGroupData, ...
            'String', {['Mean = ', num2str(mean(h.dataPerMouse)), ...
            ', STD = ', num2str(std(h.dataPerMouse))]})
        set(h.ShowGroupControl, ...
            'String', {['Mean = ', num2str(mean(h.ControldataPerMouse)), ...
            ', STD = ', num2str(std(h.ControldataPerMouse))]})
        warning off
        legend(h.StatisticAxes, 'Control', 'Data');
        warning on

    end

% Save all results to disk
% -------------------------------------------------------------------------
    function SaveAllBtn_Callback(~, ~)
        % Get user selected folder
        outDir = uigetdir(h.pathname, 'Pick a Directory');

        % Set variables to be saved
        ControldataOut = h.ControldataOut(:); % Data from control images
        dataOut = h.dataOut(:); % Data from experimantal images

        % Save control and experimantal data in the user defined derictory
        % as matlab files
        save(fullfile(outDir, 'Controls.mat'), 'ControldataOut');
        save(fullfile(outDir, 'Data.mat'), 'dataOut');

        % And as delimited text files
        dlmwrite(fullfile(outDir, 'Controls.dat'), ControldataOut);
        dlmwrite(fullfile(outDir, 'Data.dat'), dataOut);

    end
end
